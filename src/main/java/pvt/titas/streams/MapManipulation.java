package pvt.titas.streams;

import java.util.*;
import java.util.stream.Collectors;

public class MapManipulation {

    public static void main(String[] args) {
        final Map<String, Integer> wordCounts = new HashMap<>();
        wordCounts.put("USA", 100);
        wordCounts.put("jobs", 200);
        wordCounts.put("software", 50);
        wordCounts.put("technology", 70);
        wordCounts.put("opportunity", 200);

        final Map<String, Integer> sortedByCount = wordCounts.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

    }

    private static List < Song > sortedSongsByYearArtistAlbum(
            final List< Song > songsToSort) {
        return songsToSort.stream()
                .sorted(
                        Comparator.comparingInt(Song::getYear)
                                .thenComparing(Song::getArtist)
                                .thenComparing(Song::getAlbum))
                .collect(Collectors.toList());
    }

}
