package pvt.titas.streams;

public class Song {

    /**
     * Song title.
     */
    private final String title;
    /**
     * Album on which song was originally included.
     */
    private final String album;
    /**
     * Song's artist.
     */
    private final String artist;
    /**
     * Year song was released.
     */
    private final int year;

    public Song(final String newTitle, final String newAlbum,
                final String newArtist, final int newYear) {
        title = newTitle;
        album = newAlbum;
        artist = newArtist;
        year = newYear;
    }

    public String getTitle() {
        return title;
    }

    public String getAlbum() {
        return album;
    }

    public String getArtist() {
        return artist;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "'" + title + "' (" + year + ") from '" + album + "' by " + artist;
    }
}
