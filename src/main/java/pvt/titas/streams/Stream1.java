package pvt.titas.streams;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Stream1 {

    public static void main(String[] args) {

        Stream<Song> songStream = Stream.empty();

        IntStream oneTwoThree = IntStream.of(1, 2, 3);
        IntStream positiveSingleDigits = IntStream.rangeClosed(1, 9);
        IntStream powersOfTwo = IntStream.iterate(1, i -> i * 2);
    }
}
