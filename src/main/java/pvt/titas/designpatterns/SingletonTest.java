package pvt.titas.designpatterns;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;

import java.util.stream.IntStream;

public class SingletonTest {



    public static void main(String[] args) {

        IntStream.rangeClosed(0, 5).forEach(i->{
            Singleton singleton = Singleton.getInstance();
            System.out.println("Call "+i+" returned an instance with hashcode: "+singleton.hashCode()+" and id: "+singleton.toString());
        });
    }
}
