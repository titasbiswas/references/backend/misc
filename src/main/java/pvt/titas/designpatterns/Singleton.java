package pvt.titas.designpatterns;

public class Singleton {

    private Singleton(){
        System.out.println("Singleton private Constructor Called");
    }

    private static class Singleton_Instance{
        private static final Singleton instance = new Singleton();
    }

    public static Singleton getInstance(){
        return Singleton_Instance.instance;
    }
}
