package pvt.titas.queues;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.StringTokenizer;

public class PriorityQwithComparator {

	// private final static Scanner scan = new Scanner(System.in);
	private static Scanner scan;
	private final static Priorities priorities = new Priorities();

	public static void main(String[] args) throws Exception {
		scan = new Scanner(new File("input00_1.txt"));
		int totalEvents = Integer.parseInt(scan.nextLine());

		List<String> events = new ArrayList<>();

		while (totalEvents-- != 0) {
			String event = scan.nextLine();
			events.add(event);
		}

		List<Student> students = priorities.getStudents(events);

		if (students.isEmpty()) {
			System.out.println("EMPTY");
		} else {
			for (Student st : students) {
				System.out.println(st.getName());
			}
		}

		//Class myClass = Class.forName("Solution");
		//Field myField = myClass.getDeclaredField("in");

		// myField.

	}

}

class Priorities {
	Comparator<Student> comp = Comparator
			.comparingDouble(Student::getCGPA).reversed()
			.thenComparing(Student::getName)
			.thenComparingInt(Student::getID);
	PriorityQueue<Student> sdpq = new PriorityQueue<>(comp);
	List<Student> lst = new ArrayList<>();

	StringTokenizer st;
	Student sd;

	List<Student> getStudents(List<String> events) {
		events.forEach(s -> {
			st = new StringTokenizer(s);
			String eventName = st.nextToken();
			if (eventName.equals("SERVED")) {
				sdpq.remove();
			} else if (eventName.equals("ENTER")) {
				String name = st.nextToken();
				double cgpa = Double.valueOf(st.nextToken());
				int id = Integer.parseInt(st.nextToken());
				sd = new Student(id, name, cgpa);
				sdpq.add(sd);
			}
		});
		
		sdpq.forEach(s -> System.out.println(s.getName() + " " + s.getCGPA() + " " + s.getID()));

		while(!sdpq.isEmpty()) {
			lst.add(sdpq.poll());
		}
		
		return lst;
	}

}

class Student {
	private int id;
	private String name;
	private double cgpa;

	Student(int id, String name, double cgpa) {
		this.id = id;
		this.name = name;
		this.cgpa = cgpa;
	}

	int getID() {
		return id;
	}

	String getName() {
		return name;
	}

	double getCGPA() {
		return cgpa;
	}
}
