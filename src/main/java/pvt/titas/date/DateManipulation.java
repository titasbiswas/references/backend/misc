package pvt.titas.date;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DateManipulation {

	static int startYear = 2019;
	static int endYear = 2024;
	static int dayBefore = 5;
	static int dayAfter = 3;
	static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
	static List<String> result = new ArrayList<>();

	public static void main(String[] args) {

		IntStream.rangeClosed(startYear, endYear).mapToObj(y -> {
			LocalDate dt = LocalDate.ofYearDay(y, 1);
			return dt;
		}).forEach(dt -> {
			IntStream.rangeClosed(1, 12).forEach(m -> {
				LocalDate dm = dt.withMonth(m);
				LocalDate lastDay = dm.with(TemporalAdjusters.lastDayOfMonth());
				result.add(dtf.format(lastDay.minusDays(dayBefore)) + "-" + dtf.format(lastDay.plusDays(dayAfter)));
			});
		});

		System.out.println(result.stream().collect(Collectors.joining(",")).toString());
	}

}
