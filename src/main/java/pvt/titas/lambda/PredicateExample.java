package pvt.titas.lambda;

import javafx.application.Application;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class PredicateExample extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        Predicate<String> checkLength = str -> str.length()<5;

        List<String> lst = Arrays.asList("aabraa", "kaa", "daabraa");

        System.out.println(checkLength.test("Jonathan"));
        lst.stream().filter(checkLength).forEach(System.out::println);

    }
}
