package pvt.titas.lambda;

import java.util.function.Function;

public class LambdaRecursion {

    final Function<Integer, Integer> factorial = x -> x == 1
            ? x
            : x * this.factorial.apply(x - 1);

    static final Function<Integer, Integer> factorialStat = x -> x == 1
            ? x
            : x * LambdaRecursion.factorialStat.apply(x - 1);

    public static void main(String[] args) {
        System.out.println(factorialStat.apply(16));
    }
}
