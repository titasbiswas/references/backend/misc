package pvt.titas.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

//A Consumer accepts one argument and doesn't return any value, but intended to have side effects.
//Got one functiona - accept and one default method andThen()
public class ConsumerExamples {
    public static void main(String[] args) {
        List<Integer> lst = Arrays.asList(1, 3, 5, 7, 9);

        Consumer<List<Integer>> doubler = list -> {
            //list.replaceAll(e -> e*2);
            for (int i = 0; i < list.size(); i++) {
                list.set(i, list.get(i) * 2);
            }
        };

        Consumer<List<Integer>> printer = list -> list.forEach(System.out::println);

        Consumer<List<Integer>> incrementer = list -> list.replaceAll(e -> e + 10);
        System.out.println("Single consumer operation");
        doubler.andThen(incrementer).andThen(printer).accept(lst);
        System.out.println("Single consumer operation with error");

        //Exception Handling
        try {
            doubler.andThen(null).accept(lst);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // BI-Consumer
        List<Integer> lst2 = Arrays.asList(0, 2, 4, 6, 8, 10, 12, 14, 16, 18);
        List<Integer> lst1 = Arrays.asList(1, 3, 5, 7, 9);


        BiConsumer<List<Integer>, List<Integer>> combiner = (list1, list2) -> {
            List<Integer> list = new ArrayList<>();
            int i = 0, j = 0, l1 = list1.size(),
                    l2 = list2.size();

            while (i < l1 && j < l2) {
                if (list1.get(i) <= list2.get(j)) {
                    list.add(list1.get(i));
                    i++;
                } else {
                    list.add(list2.get(j));
                    j++;
                }
            }

            while (j < l2) {
                list.add(list2.get(j));
                j++;
            }

            while (i < l1) {
                list.add(list1.get(i));
                i++;
            }

            list.forEach(System.out::println);

        };
        System.out.println("Bi consumer operation");

        combiner.accept(lst1, lst2);

    }
}
